﻿using Marketing.Entities.SIM;
using Marketing.Helper;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Dynamic;
using Marketing.Models.Kerjasama;
//using static Marketing.Models.Kerjasama.SetupKontrakMCUModel;

namespace Marketing.Controllers.Kerjasama
{
    [Authorize(Roles = "Marketing")]
    public class SetupKontrakMCUController : Controller
    {
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.MR_ListKontrakMCU.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y => y.Kode_Customer.Contains(x.Value) ||
                            y.Nama_Customer.Contains(x.Value) ||
                            y.Alamat_1.Contains(x.Value)
                           );

                        //if (x.Key == "Kategori" && !string.IsNullOrEmpty(x.Value))
                        //{
                        //    proses = proses.Where(y => y.KatVendor_Kategori_Name.Contains(x.Value)
                        //        );
                        //}
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "DESC" : "ASC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        CustomerID = x.Customer_ID,
                        Kode_Customer = x.Kode_Customer,
                        Nama_Customer = x.Nama_Customer,
                        Alamat_1 = x.Alamat_1,
                        No_Telepon_1 = x.No_Telepon_1,
                        StartDate = x.StartDate.Value.ToString("yyyy-MM-dd"),
                        EndDate = x.EndDate.Value.ToString("yyyy-MM-dd"),
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, SetupKontrakMCUModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var id = 0;
                        var userid = User.Identity.GetUserId();
                        if (_process == "CREATE")
                        {
                            var kontrakmcu = s.SIMdKontrakMCU.Where(x => x.Customer_ID == model.CustomerID && x.IDJasa == model.IDJasa);
                            if (kontrakmcu.Count() > 0) {
                                return JsonConvert.SerializeObject(new { IsSuccess = false, Message = "o"});
                            }
                            if (model.DetailKontrak == null) model.DetailKontrak = new List<SetupKontrakMCUDetailModel>();
                            if (model.DetailAnggota == null) model.DetailAnggota = new List<SetupAnggotaDetailModel>();
                            id = s.MR_InsertHeaderKontrakMCU(
                                model.CustomerID,
                                model.IDJasa,
                                model.Harga,
                                model.StartDate,
                                model.EndDate,
                                model.Diskon_Total,
                                model.Diskon_Detail,
                                model.Pembayaran_Tunai,
                                model.Pembayaran_Perusahaan,
                                model.Pembayaran_HC,
                                model.Active
                            );
                            foreach (var m in model.DetailKontrak)
                            {
                                s.MR_InsertDetailKontrakMCU(
                                    model.CustomerID,
                                    m.IDJasa,
                                    m.SectionID,
                                    m.Harga
                                    );
                            }

                            foreach (var m in model.DetailAnggota)
                            {
                                s.MR_InsertAnggotaKontrakMCU(
                                    model.CustomerID,
                                    m.IDJasa,
                                    m.NoAnggota,
                                    m.Nama,
                                    m.Gender,
                                    m.Alamat,
                                    m.Phone,
                                    m.MobilePhone,
                                    m.Klp,
                                    m.JabatanPerusahaan,
                                    m.Active
                                   );
                            }
                            s.SaveChanges();
                        }
                        else if (_process == "EDIT")
                        {
                            id = model.CustomerID;
                            if (model.DetailKontrak == null) model.DetailKontrak = new List<SetupKontrakMCUDetailModel>();
                            if (model.DetailAnggota == null) model.DetailAnggota = new List<SetupAnggotaDetailModel>();
                            s.MR_UpdateHeaderKontrakMCU (
                                model.CustomerID,
                                model.IDJasa,
                                model.Harga,
                                model.StartDate,
                                model.EndDate,
                                model.Diskon_Total,
                                model.Diskon_Detail,
                                model.Pembayaran_Tunai,
                                model.Pembayaran_Perusahaan,
                                model.Pembayaran_HC,
                                model.Active
                            );

                            var dKontrak = s.MR_GetDetailKontrakMCU.Where(x => x.CustomerID == id).ToList();
                            var dAnggota = s.MR_GetAnggotaKontrakMCU.Where(x => x.Customer_ID == id).ToList();
                            foreach (var x in dKontrak)
                            {
                                var _d = model.DetailKontrak.FirstOrDefault(y => y.IDJasa == x.IDJasa);
                                
                            }
                            foreach (var x in dAnggota)
                            {
                                var _d = model.DetailAnggota.FirstOrDefault(y => y.IDJasa == x.IDJasa);
                               
                            }
                            foreach (var x in model.DetailKontrak)
                            {
                                var _d = dKontrak.FirstOrDefault(y => y.IDJasa == x.IDJasa);
                                if (_d == null)
                                {
                                    // new
                                    s.MR_InsertDetailKontrakMCU((short)id, x.IDJasa, x.SectionID, x.Harga);
                                }
                                else
                                {
                                    // edit
                                    s.MR_UpdateDetailKontrakMCU((short)id, x.IDJasa, x.IDJasaLama, x.SectionID);
                                }

                            }

                            foreach (var x in model.DetailAnggota)
                            {
                                var _d = dAnggota.FirstOrDefault(y => y.IDJasa == x.IDJasa);
                                if (_d == null)
                                {
                                    // new
                                    s.MR_InsertAnggotaKontrakMCU((short)id, x.IDJasa, x.NoAnggota, x.Nama, x.Gender, x.Alamat, x.Phone, x.MobilePhone, x.Klp, x.JabatanPerusahaan, x.Active);
                                }
                                else
                                {
                                    // edit
                                    s.MR_UpdateAnggotaKontrakMCU((short)id, x.IDJasa, x.NoAnggota, x.Nama, x.Gender, x.Alamat, x.Phone, x.MobilePhone, x.Klp, x.JabatanPerusahaan, x.Active);
                                }

                            }
                            s.SaveChanges();
                        }
                        //else if (_process == "DELETE")
                        //{
                        //    id = model.Supplier_ID;
                        //    s.ADM_DeleteVendor(id.ToString());
                        //    s.SaveChanges();
                        //}

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"SetupVendor-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
            return HConvert.Success();
        }

        [HttpPost]
        public string Detail(short id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_GetHeaderKontrakMCU.FirstOrDefault(x => x.Customer_ID == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var dKontrak = s.MR_GetDetailKontrakMCU.Where(x => x.CustomerID == m.Customer_ID).OrderBy(x => x.IDJasa).ToList();
                    var dAnggota = s.MR_GetAnggotaKontrakMCU.Where(x => x.Customer_ID == m.Customer_ID).OrderBy(x => x.IDJasa).ToList();
                    var dd = s.MR_GetDetailKomponenKontrakMCU.Where(x => x.CustomerID == m.Customer_ID).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            Customer_ID = m.Customer_ID,
                            Kode_Customer = m.Kode_Customer,
                            Nama_Customer = m.Nama_Customer,
                            Alamat_1 = m.Alamat_1,
                            No_Telepon_1 = m.No_Telepon_1,
                            StartDate = m.StartDate.Value.ToString("yyyy-MM-dd"),
                            EndDate = m.EndDate.Value.ToString("yyyy-MM-dd"),
                            MCUID = m.IDJasa,
                            //SebelumDiskon = m.,
                            Diskon_Detail = m.Diskon_Detail,
                            Diskon_Total = m.Diskon_Total,
                            //SetelahDiskon = m.,
                            Pembayaran_Tunai = m.Pembayaran_Tunai,
                            Pembayaran_Perusahaan = m.Pembayaran_Perusahaan,
                            SHHC = m.Pembayaran_HC,
                            //Total = m.,
                        },
                        DetailKontrak = dKontrak.ConvertAll(x => new
                        {
                            Customer_ID = m.Customer_ID,
                            IDJasa = x.IDJasa,
                            JasaName = x.JasaName,
                            SectionName = x.SectionName,
                            Harga_Baru = x.Harga_Baru,
                        }),
                        DetailAnggota = dAnggota.ConvertAll(x => new
                        {
                            Customer_ID = m.Customer_ID,
                            NoAnggota = x.NoAnggota,
                            Nama = x.Nama,
                            Gender = x.Gender,
                            ALamat = x.ALamat,
                            Phone = x.Phone,
                            MobilePhone = x.MobilePhone,
                            Active = x.Active,
                        }),
                        DetailDetail = dd.ConvertAll(x => new {
                            JasaId = x.IDJasaInclude,
                            Id = x.IDKomponenID,
                            Nama = x.KomponenName,
                            Harga = x.Harga
                        })
                    });;
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P - P E R U S A H A A N

        [HttpPost]
        public string ListLookupPerusahaan(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.MR_GetPerusahaanKontrakMCU.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.Kode_Customer.Contains(x.Value) ||
                                y.Nama_Customer.Contains(x.Value) ||
                                y.No_Telepon_1.Contains(x.Value) ||
                                y.Alamat_1.Contains(x.Value));
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        CustomerID = x.Customer_ID,
                        Kode_Customer = x.Kode_Customer,
                        Nama_Customer = x.Nama_Customer,
                        Alamat_1 = x.Alamat_1,
                        No_Telepon_1 = x.No_Telepon_1,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P - M C U J A S A

        [HttpPost]
        public string ListLookupJasa(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.MR_GetJasaKontrakMCU.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.JasaID.Contains(x.Value) ||
                                y.JasaName.Contains(x.Value));
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        Id = x.JasaID,
                        Nama = x.JasaName,
                        Harga = x.Harga_Lama
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string GetJasa(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_ListDetailPaketMCU.Where(x => x.MCUID == id).FirstOrDefault();
                    if (m == null) throw new Exception("Detail tidak boleh kosong");
                    var d = s.MR_ListKomponenPaketMCU.Where(x => x.MCUID == id && x.JasaID == m.JasaID).ToList();
                    var r = new
                    {
                        Id = m.JasaID,
                        Nama = m.JasaName,
                        Harga = m.Harga,
                        SectionID = m.SectionID,
                        SectionName = m.SectionName,
                        
                        Detail = d.ConvertAll(x => new {
                            JasaId = x.MCUID,
                            Id = x.JasaID,
                            Nama = x.KomponenName,
                            Harga = x.Harga
                        })
                    };
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P - A N G G O T A

        [HttpPost]
        public string ListLookupAnggota(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.MR_GetAnggotaKontrakMCU.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.NoAnggota.Contains(x.Value) ||
                                y.Nama.Contains(x.Value) ||
                                y.Gender.Contains(x.Value) ||
                                y.ALamat.Contains(x.Value) ||
                                y.Phone.Contains(x.Value) ||
                                y.MobilePhone.Contains(x.Value));
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        NoAnggota = x.NoAnggota,
                        Nama = x.Nama,
                        Gender = x.Gender,
                        ALamat = x.ALamat,
                        Phone = x.Phone,
                        MobilePhone = x.MobilePhone,
                        Active = x.Active,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== GET JASA PAKET

        [HttpPost]
        public string GetJasaPaketMCU(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var mm = s.MR_ListDetailPaketMCU.FirstOrDefault(x => x.MCUID == id);
                    var dpaketMCU = s.ADM_GetPaketMCU.Where(x => x.MCUID == id).ToList();
                    var dpaketdetailMCU = s.MR_ListDetailPaketMCU.Where(y => y.MCUID == id).ToList();
                    var dpaketkomponenMCU = s.MR_ListKomponenPaketMCU.Where(z => z.MCUID == id && z.JasaID == mm.JasaID).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        detailMCUDetail = dpaketdetailMCU.ConvertAll(y => new
                        {
                            MCUID = y.MCUID,
                            JasaID = y.JasaID,
                            JasaName = y.JasaName,
                            Harga = y.Harga,
                            SectionName = y.SectionName,
                            SectionID = y.SectionID,

                        }),
                        DetailKomponen = dpaketkomponenMCU.ConvertAll(z => new
                        {
                            Id = z.MCUID,
                            JasaId = z.JasaID,
                            Nama = z.KomponenName,
                            Harga = z.Harga
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string GetJasaPaketMCUKomponen(string id, string jasaid)
        {
            try
            {
                using (var s = new SIMEntities())
                {
               
                    var dpaketkomponenMCU = s.MR_ListKomponenPaketMCU.Where(z => z.MCUID == id && z.JasaID == jasaid).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        DetailKomponen = dpaketkomponenMCU.ConvertAll(z => new
                        {
                            Id = z.MCUID,
                            JasaId = z.JasaID,
                            Nama = z.KomponenName,
                            Harga = z.Harga
                        }),
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }
        #endregion
    }
}
