﻿using Marketing.Entities.SIM;
using Marketing.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Marketing.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var model = new HomeViewModel();
            
            try
            {
                using (var s = new SIMEntities())
                {
                    var start = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                    var end = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month));

                    return View(model);
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public ActionResult ApplicationInfo()
        {
            return View();
        }

        public ActionResult Help()
        {
            return View();
        }
    }
}