﻿using Marketing.Entities.SIM;
using Marketing.Helper;
using Marketing.Models.Setup;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Marketing.Controllers.Setup
{
    [Authorize(Roles = "Marketing")]
    public class DepartemenController : Controller
    {
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.MR_ListDepartemen.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.Nama_Customer.Contains(x.Value) ||
                                y.Alamat_1.Contains(x.Value) ||
                                y.No_Telepon_1.Contains(x.Value) ||
                                y.No_Fax.Contains(x.Value));
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.Customer_ID,
                        Kode = m.Kode_Customer,
                        Nama= m.Nama_Customer,
                        Alamat = m.Alamat_1,
                        Phone = m.No_Telepon_1,
                        NoFax = m.No_Fax
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, DepartemenModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        int id = 0;
                        if (_process == "EDIT")
                        {
                            if (model.Detail == null) model.Detail = new List<DepartemenDetailModel>();
                            //if (model.Detail.GroupBy(x => x.Nama).Where(x => x.Count() > 1).Count() > 0)
                            //    throw new Exception("Nama Detail Kontak tidak boleh sama");

                            s.MR_UpdateHeaderDepartemen(
                                model.Id,
                                model.Nama,
                                model.Alamat,
                                model.Phone,
                                model.NoFax,
                                model.Aktif
                            );

                            var d = s.MR_ListDetailDepartemen.Where(x => x.Customer_ID == model.Id).ToList();
                            foreach (var x in d)
                            {
                                var _d = model.Detail.FirstOrDefault(y => y.Nama == x.NamaDepartemen);
                                //delete
                                if (_d == null) s.MR_DeleteDetailDepartemen((short)model.Id, x.NamaDepartemen);
                            }
                            foreach (var x in model.Detail)
                            {
                                var _d = d.FirstOrDefault(y => y.NamaDepartemen == x.Nama);
                                if (_d == null)
                                {
                                    // new
                                    s.MR_InsertDetailDepartemen(
                                        (short)model.Id,
                                        x.Nama
                                    );
                                }
                            }
                            s.SaveChanges();
                            id = model.Id;
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Departemen-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string Detail(int id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_ListDepartemen.FirstOrDefault(x => x.Customer_ID == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.MR_ListDetailDepartemen.Where(x => x.Customer_ID == m.Customer_ID).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            Id = m.Customer_ID,
                            Kode = m.Kode_Customer,
                            Nama = m.Nama_Customer,
                            Alamat = m.Alamat_1,
                            Phone = m.No_Telepon_1,
                            NoFax = m.No_Fax,
                            Aktif = m.aktif
                        },
                        Detail = d.ConvertAll(x => new
                        {
                            Nama = x.NamaDepartemen,
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K - U P FORMULARIUM

        [HttpPost]
        public string List_Departemen(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.MR_GetDepartemenAnggota.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.Departemen.Contains(x.Value));
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.Departemen,
                        Departemen = m.Departemen
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}