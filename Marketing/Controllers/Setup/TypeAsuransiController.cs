﻿using Marketing.Entities.SIM;
using Marketing.Helper;
using Marketing.Models.Setup;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Marketing.Controllers.Setup
{
    [Authorize(Roles = "Marketing")]
    public class TypeAsuransiController : Controller
    {
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.MR_ListTypeAsuransi.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.Catatan.Contains(x.Value) ||
                                y.Deskripsi.Contains(x.Value) ||
                                y.TypeAsuransiID.Contains(x.Value));
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.TypeAsuransiID,
                        Deskripsi = m.Deskripsi,
                        Catatan = m.Catatan
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, TypeAsuransiModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        string id = "";
                        if (_process == "CREATE")
                        {
                            if (model.Detail == null) model.Detail = new List<TypeAsuransiDetailModel>();
                            if (model.Detail.GroupBy(x => new { x.TipePasien, x.RangeMin }).Where(x => x.Count() > 1).Count() > 0)
                                throw new Exception("Detail dengan identifikasi yang sama sudah ada");

                            id = s.MR_InsertHeaderTypeAsuransi(
                                model.Deskripsi,
                                model.Catatan
                            ).FirstOrDefault();
                            foreach (var x in model.Detail)
                            {
                                s.MR_InsertDetailTypeAsuransi(
                                    id,
                                    x.TipePasien,
                                    x.RangeMin,
                                    x.RangeMax,
                                    x.ProsenUp
                                );
                            }
                            s.SaveChanges();
                        }
                        else if (_process == "EDIT")
                        {
                            if (model.Detail == null) model.Detail = new List<TypeAsuransiDetailModel>();
                            if (model.Detail.GroupBy(x => new { x.TipePasien, x.RangeMin }).Where(x => x.Count() > 1).Count() > 0)
                                throw new Exception("Detail dengan identifikasi yang sama sudah ada");

                            s.MR_UpdateHeaderTypeAsuransi(
                                model.Id,
                                model.Deskripsi,
                                model.Catatan
                            );

                            var d = s.MR_GetDetailTypeAsuransi.Where(x => x.TypeAsuransiID == model.Id).ToList();
                            foreach (var x in d)
                            {
                                var _d = model.Detail.FirstOrDefault(y => y.TipePasien == x.JenisPasienID && y.RangeMin == x.Range_Min);
                                //delete
                                //if (_d == null) s.MR_DeleteDetailTypeAsuransi((short)model.Id, x.KontakPerson_ID);
                            }
                            foreach (var x in model.Detail)
                            {
                                var _d = d.FirstOrDefault(y => y.TypeAsuransiID == model.Id);
                                if (_d == null)
                                {
                                    // new
                                    s.MR_InsertDetailTypeAsuransi(
                                        model.Id,
                                        x.TipePasien,
                                        x.RangeMin,
                                        x.RangeMax,
                                        x.ProsenUp
                                    );
                                }
                                else
                                {
                                    // edit
                                    s.MR_UpdateDetailTypeAsuransi(
                                        model.Id,
                                        x.TipePasien,
                                        x.RangeMin,
                                        x.RangeMax,
                                        x.ProsenUp
                                    );
                                }

                            }
                            s.SaveChanges();
                            id = model.Id;
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"SetupTypeAsuransi-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string Detail(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_ListTypeAsuransi.FirstOrDefault(x => x.TypeAsuransiID == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.MR_GetDetailTypeAsuransi.Where(x => x.TypeAsuransiID == m.TypeAsuransiID).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            Id = m.TypeAsuransiID,
                            Deskripsi = m.Deskripsi,
                            Catatan = m.Catatan
                        },
                        Detail = d.ConvertAll(x => new
                        {
                            TipePasien = x.JenisPasienID,
                            RangeMin = x.Range_Min,
                            RangeMax = x.Range_Max,
                            ProsenUp = x.Prosen_Up
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}