﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Marketing.Entities.SIM;
using Newtonsoft.Json;

namespace Marketing.Helper
{
    public static class HList
    {
        public static List<SelectListItem> ListBulan
        {
            get
            {
                return new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "Januari", Value = "1" },
                    new SelectListItem(){ Text = "Febuari", Value = "2" },
                    new SelectListItem(){ Text = "Maret", Value = "3" },
                    new SelectListItem(){ Text = "April", Value = "4" },
                    new SelectListItem(){ Text = "Mei", Value = "5" },
                    new SelectListItem(){ Text = "Juni", Value = "6" },
                    new SelectListItem(){ Text = "Juli", Value = "7" },
                    new SelectListItem(){ Text = "Agustus", Value = "8" },
                    new SelectListItem(){ Text = "September", Value = "9" },
                    new SelectListItem(){ Text = "Oktober", Value = "10" },
                    new SelectListItem(){ Text = "November", Value = "11" },
                    new SelectListItem(){ Text = "Desember", Value = "12" },
                };
            }
        }

        public static string Setup_SetupPerusahaanKerjasama_Kategori
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_GetKategoriPerusahaanKerjasama.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Kode_Kategori}\">{x.Kategori_Name}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Setup_SetupPerusahaanKerjasama_TipePembayaran
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_GetTipePembayaran.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.IDBayar}\">{x.TipePembayaran}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Setup_SetupPerusahaanKerjasama_MataUang
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_GetMataUang.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Currency_ID}\">{x.MataUang}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Setup_JenisPasien_Tipe
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_GetTipeJenisPasien.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Tipe}\">{x.Tipe}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Setup_JenisPasien_JenisPembayaran
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_GetJenisPembayaran.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.IDBayar}\">{x.Description}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Setup_TypeAsurasnsi_TipePasien
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_GetJenisPasienTypeAsuransi.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.JenisKerjasamaID}\">{x.JenisKerjasama}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Setup_NamaAsuransi_TypeAsuransi
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_ListTypeAsuransi.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.TypeAsuransiID}\">{x.Deskripsi}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Kerjasama_SetupAnggotaKerjasama_KPL
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_GetKelompokAnggota.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Klp}\">{x.Klp}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Kerjasama_SetupAnggotaKerjasama_Departemen
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_GetDepartemenAnggota.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Departemen}\">{x.Departemen}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Kerjasama_SetupKontrakKerjasama_JenisKerjasama
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_GetDepartemenAnggota.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Departemen}\">{x.Departemen}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Kerjasama_SetupKontrakKerjasama_KelasPelayanan
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_GetDepartemenAnggota.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Departemen}\">{x.Departemen}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Kerjasama_SetupKontrakKerjasama_KelasKontrak
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_GetDepartemenAnggota.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Departemen}\">{x.Departemen}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Kerjasama_SetupKontrakKerjasama_Ditanggung
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_GetDepartemenAnggota.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Departemen}\">{x.Departemen}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Kerjasama_SetupKontrakKerjasama_KelebihanPlafon
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_GetDepartemenAnggota.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Departemen}\">{x.Departemen}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Kerjasama_SetupKontrakKerjasama_JenisFormularium
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_GetDepartemenAnggota.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Departemen}\">{x.Departemen}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Kerjasama_SetupKontrakKerjasama_JenisPembayaran
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_GetDepartemenAnggota.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Departemen}\">{x.Departemen}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Kerjasama_SetupKontrakKerjasama_PaketKerjasama
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_GetDepartemenAnggota.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Departemen}\">{x.Departemen}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Setup_MCU
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_ListPaketMCU.ToList();
                    string r = "";
                    r += $"<option value=\"\">-</option>";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.JasaID}\">{x.JasaName}</option>";
                    }
                    return r;
                }
            }
        }
        public static string List_Kelas
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.SIMmKelas.ToList();
                    string r = "";
                    //r += $"<option value=\"\">-</option>";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.KelasID}\">{x.NamaKelas}</option>";
                    }
                    return r;
                }
            }
        }

        public static string List_Section
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_GetSection.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.SectionID}\">{x.SectionName}</option>";
                    }
                    return r;
                }
            }
        }

        public static string List_JenisKerjasama
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_GetJenisKerjasama.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.JenisKerjasamaID}\">{x.JenisKerjasama}</option>";
                    }
                    return r;
                }
            }
        }

        public static string List_KelasPelayanan
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_GetKelasPelayanan.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.KelasID}\">{x.NamaKelas}</option>";
                    }
                    return r;
                }
            }
        }

        public static string List_Ditanggung
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_GetDitanggung.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Ditanggung}\">{x.Ditanggung}</option>";
                    }
                    return r;
                }
            }
        }

        public static string List_KelasKontrak
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_GetKelasPelayanan.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.NamaKelas}\">{x.NamaKelas}</option>";
                    }
                    return r;
                }
            }
        }
        
        public static string List_JenisFormularium
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_GetJenisFormularium.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.JenisFormularium}\">{x.JenisFormularium}</option>";
                    }
                    return r;
                }
            }
        }
        
        public static string List_PaketKerjasama
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_GetPaketKontrakKerjasama.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.NamaPaket}\">{x.NamaPaket}</option>";
                    }
                    return r;
                }
            }
        }

        public static string List_JenisPembayaran
        {
            get
            {
                var m = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "Dijamin RS", Value = "1" },
                    new SelectListItem(){ Text = "Dijamin Ke Perusahaan", Value = "2" },
                    new SelectListItem(){ Text = "Tunai", Value = "3" },
                };
                string r = "";
                foreach (var x in m)
                {
                    r += $"<option value=\"{x.Value}\">{x.Text}</option>";
                }
                return r;
            }
        }

        public static string List_TglJatuhTempo
        {
            get
            {
                var m = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "1", Value = "1" },
                    new SelectListItem(){ Text = "2", Value = "2" },
                    new SelectListItem(){ Text = "3", Value = "3" },
                    new SelectListItem(){ Text = "4", Value = "4" },
                    new SelectListItem(){ Text = "5", Value = "5" },
                    new SelectListItem(){ Text = "6", Value = "6" },
                    new SelectListItem(){ Text = "7", Value = "7" },
                    new SelectListItem(){ Text = "8", Value = "8" },
                    new SelectListItem(){ Text = "9", Value = "9" },
                    new SelectListItem(){ Text = "10", Value = "10" },
                    new SelectListItem(){ Text = "11", Value = "11" },
                    new SelectListItem(){ Text = "12", Value = "12" },
                    new SelectListItem(){ Text = "13", Value = "13" },
                    new SelectListItem(){ Text = "14", Value = "14" },
                    new SelectListItem(){ Text = "15", Value = "15" },
                    new SelectListItem(){ Text = "16", Value = "16" },
                    new SelectListItem(){ Text = "17", Value = "17" },
                    new SelectListItem(){ Text = "18", Value = "18" },
                    new SelectListItem(){ Text = "19", Value = "19" },
                    new SelectListItem(){ Text = "20", Value = "20" },
                    new SelectListItem(){ Text = "21", Value = "21" },
                    new SelectListItem(){ Text = "22", Value = "22" },
                    new SelectListItem(){ Text = "23", Value = "23" },
                    new SelectListItem(){ Text = "24", Value = "24" },
                    new SelectListItem(){ Text = "25", Value = "25" },
                    new SelectListItem(){ Text = "26", Value = "26" },
                    new SelectListItem(){ Text = "27", Value = "27" },
                    new SelectListItem(){ Text = "28", Value = "28" },
                };
                string r = "";
                foreach (var x in m)
                {
                    r += $"<option value=\"{x.Value}\">{x.Text}</option>";
                }
                return r;
            }
        }

        public static string List_ObatDitanggung
        {
            get
            {
                var m = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "-", Value = "0" },
                    new SelectListItem(){ Text = "ALL", Value = "1" },
                    new SelectListItem(){ Text = "RI Saja", Value = "2" },
                    new SelectListItem(){ Text = "RJ Saja", Value = "3" },
                    
                };
                string r = "";
                foreach (var x in m)
                {
                    r += $"<option value=\"{x.Value}\">{x.Text}</option>";
                }
                return r;
            }
        }

        public static string List_HargaObatDigunakan
        {
            get
            {
                var m = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "-", Value = "0" },
                    new SelectListItem(){ Text = "Default", Value = "1" },
                    new SelectListItem(){ Text = "Harga Umum", Value = "2" },
                    new SelectListItem(){ Text = "Harga Executive Non KITAS", Value = "3" },
                    new SelectListItem(){ Text = "Harga Executive KITAS", Value = "4" },
                    new SelectListItem(){ Text = "Harga IKS", Value = "4" },
                    new SelectListItem(){ Text = "Harga HC", Value = "5" },

                };
                string r = "";
                foreach (var x in m)
                {
                    r += $"<option value=\"{x.Value}\">{x.Text}</option>";
                }
                return r;
            }
        }

        public static string List_KelebihanPlafon
        {
            get
            {
                var m = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "-", Value = "0" },
                    new SelectListItem(){ Text = "Peribadi", Value = "1" },
                    new SelectListItem(){ Text = "Perusahaan", Value = "2" },

                };
                string r = "";
                foreach (var x in m)
                {
                    r += $"<option value=\"{x.Value}\">{x.Text}</option>";
                }
                return r;
            }
        }

    }
}