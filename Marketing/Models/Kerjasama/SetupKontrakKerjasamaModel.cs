﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Marketing.Models.Kerjasama
{
    public class SetupKontrakKerjasamaModel
    {
        public Nullable<short> IdPerusahaan { get; set; }
        public decimal CustomerKerjasamaID { get; set; }
        public string KodePerusahaan { get; set; }
        public string NamaPerusahaan { get; set; }
        public string AlamatPerusahaan { get; set; }
        public Nullable<int> JenisKerjasama { get; set; }
        public string KelasPelayanan { get; set; }
        public string KelasKontrak { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<decimal> CoPay { get; set; }
        public string Ditanggung { get; set; }
        public string KelebihanPlafon { get; set; }
        public Nullable<decimal> IuranPerAnggota { get; set; }
        public Nullable<byte> TglJatuhTempo { get; set; }
        public string JenisFormularium { get; set; }
        //public string AssistingCompany { get; set; }
        //public string Nama { get; set; }
        //public string Alamat { get; set; }
        public Nullable<short> MaxRawatOpname1 { get; set; }
        public Nullable<short> MaxRawatOpname2 { get; set; }
        public Nullable<short> MaxICUOpname1 { get; set; }
        public Nullable<short> MaxICUOpname2 { get; set; }
        public Nullable<decimal> MaxNilaiRawatInap { get; set; }
        public Nullable<double> TarifYangDigunakanUp { get; set; }
        public Nullable<double> TarifYangDigunakanDiskon { get; set; }
        public Nullable<bool> DiskonLangsungKePasien { get; set; }
        public string HargaObatDigunakan { get; set; }
        public Nullable<double> UpObat { get; set; }
        public Nullable<double> DiskonObat { get; set; }
        public Nullable<int> JenisPembayaran { get; set; }
        public Nullable<double> JenisPembayaranYangDitanggung { get; set; }
        public string PaketKerjasama { get; set; }
        public Nullable<bool> Aktif { get; set; }
        public Nullable<bool> SembunyikanObat { get; set; }
        public Nullable<bool> PaketPemeriksaan { get; set; }
        public Nullable<bool> Active { get; set; }
        public List<SetupKontrakKerjasamaDetailPlafonModel> DetailPlafon { get; set; }
        public List<SetupKontrakKerjasamaFormulariumModel> Formularium { get; set; }
        public List<SetupKontrakKerjasamaDiskonModel> Diskon { get; set; }
        public List<SetupKontrakKerjasamaSectionDitanggungModel> SectionDitanggung { get; set; }
        public List<SetupKontrakKerjasamaJasaExectionDiskonModel> JasaExectionDiskon { get; set; }
        public List<SetupKontrakKerjasamaRasioObatModel> RasioObat { get; set; }
        public List<SetupKontrakKerjasamaMarkupModel> Markup { get; set; }
        public List<SetupKontrakKerjasamaDataPaketObatModel> DataPaketObat { get; set; }
    }

    public class SetupKontrakKerjasamaDetailPlafonModel
    {
        public string KategoriPlafon { get; set; }
        public bool Ditanggung { get; set; }
        public decimal? NilaiPlafon { get; set; }
    }

    public class SetupKontrakKerjasamaFormulariumModel
    {
        public int Id { get; set; }
        public bool Formularium { get; set; }
        public bool Ditanggung { get; set; }
        public int BarangID { get; set; }
        public short JenisKerjasamaID { get; set; }
        public bool Include { get; set; }
        public bool DitanggungFormularium { get; set; }
    }

    public class SetupKontrakKerjasamaDiskonModel
    {
        public string Diskon { get; set; }
        public double? DiscRI { get; set; }
        public double? DIscRJ { get; set; }
        public string IDDiscount { get; set; }
        public double? Nilai { get; set; }
        public double? NilaiRJ { get; set; }

    }

    public class SetupKontrakKerjasamaSectionDitanggungModel
    {
        public int Id { get; set; }

        public bool DitanggungSection { get; set; }
        public short SectionID { get; set; }
    }

    public class SetupKontrakKerjasamaJasaExectionDiskonModel
    {
        public string Id { get; set; }

    }

    public class SetupKontrakKerjasamaRasioObatModel
    {
        public string Spesialis { get; set; }
        public string SubSpesialis { get; set; }
        public decimal? NilaiRasio { get; set; }
        public bool Blok { get; set; }
    }

    public class SetupKontrakKerjasamaMarkupModel
    {
        public byte GroupJasa { get; set; }
        public double? Markup { get; set; }
        public string Keterangan { get; set; }
    }

    public class SetupKontrakKerjasamaDataPaketObatModel
    {
        public string KelompokJenis { get; set; }
    }
}