﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Marketing.Models.Kerjasama
{

    public class SetupKontrakMCUModel
    {
        public short CustomerID { get; set; }
        public string IDJasa { get; set; }
        public decimal Harga { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public double Diskon_Total { get; set; }
        public double Diskon_Detail { get; set; }
        public decimal Pembayaran_Tunai { get; set; }
        public decimal Pembayaran_Perusahaan { get; set; }
        public decimal Pembayaran_HC { get; set; }
        public bool Active { get; set; }


        public List<SetupKontrakMCUDetailModel> DetailKontrak { get; set; }
        public List<SetupAnggotaDetailModel> DetailAnggota { get; set; }
    }
    public class SetupKontrakMCUDetailModel
    {
        public string IDJasa { get; set; }
        public string IDJasaLama { get; set; }
        public string SectionID { get; set; }
        public decimal Harga { get; set; }

        public List<SetupKontrakMCUDetailKomponenModel> Detail { get; set; }
    }

    public class SetupAnggotaDetailModel
    {
        public string IDJasa { get; set; }
        public string NoAnggota { get; set; }
        public decimal Nama { get; set; }
        public string Gender { get; set; }
        public string Alamat { get; set; }
        public string Phone { get; set; }
        public string MobilePhone { get; set; }
        public string Klp { get; set; }
        public string JabatanPerusahaan { get; set; }
        public bool Active { get; set; }

    }

    public class SetupKontrakMCUDetailKomponenModel
    {
        public string MCUID { get; set; }
        public string JasaID { get; set; }
        public string KomponenName { get; set; }
        public decimal Harga { get; set; }
    }


}
    