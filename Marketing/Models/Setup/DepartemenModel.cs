﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Marketing.Models.Setup
{
    public class DepartemenModel
    {
        public short Id { get; set; }
        public string Nama { get; set; }
        public string Alamat { get; set; }
        public string Phone { get; set; }
        public string NoFax { get; set; }
        public bool Aktif { get; set; }
        public List<DepartemenDetailModel> Detail { get; set; }
    }

    public class DepartemenDetailModel 
    {
        public string Nama { get; set; }
    }
}