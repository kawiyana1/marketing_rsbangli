﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Marketing.Models.Setup
{
    public class PerusahaanKerjasamaModel
    {
        public int Id { get; set; }
        public string KodeCustomer { get; set; }
        public string NamaCustomer { get; set; }
        public string Alias { get; set; }
        public string Kategori { get; set; }
        public string Alamat1 { get; set; }
        public string AlamatClaim1 { get; set; }
        public string AlamatClaim2 { get; set; }
        public string AlamatClaim3 { get; set; }
        public string KodePos { get; set; }
        public string NoTelp1 { get; set; }
        public string NoTelp2 { get; set; }
        public string NoTelp3 { get; set; }
        public string NoFax { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string NoNPWP { get; set; }
        public short? MataUang { get; set; }
        public decimal? BatasKredit { get; set; }
        public string TypePembayaran { get; set; }
        public short? TemPembayaran { get; set; }
        public int? NoAkun { get; set; }
        public bool Active { get; set; }

        public List<SetupPerusahaanKerjasamaDetailModel> Detail { get; set; }
    }

    public class SetupPerusahaanKerjasamaDetailModel 
    {
        public int Id { get; set; }
        public string Nama { get; set; }
        public string Alamat { get; set; }
        public string TelpKantor { get; set; }
        public string TelpRumah { get; set; }
        public string HP { get; set; }
        public string Fax { get; set; }
        public string TglLahir { get; set; }
        public string Jabatan { get; set; }
        public string Email { get; set; }
        public string Keterangan { get; set; }
    }
}